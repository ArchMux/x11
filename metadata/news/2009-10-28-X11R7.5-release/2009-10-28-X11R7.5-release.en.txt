Title: Notes on X11R7.5 release
Author: Ingmar Vanhassel <ingmar@exherbo.org>
Content-Type: text/plain
Posted: 2009-10-28
Revision: 2
News-Item-Format: 1.0
Display-If-Installed: x11-libs/libX11[<1.4.4]

The latest X11 release, X11R7.5 is now in ~arch.

As is common with new X11 releases, you will have to rebuild your
X11 drivers after upgrading xorg-server.

Some X11 headers were moved between packages, so some DEPENDENCIES may
have to be updated to reflect this.

With this release the default fonts path was changed to a saner
location, in /usr/share/fonts. In Exherbo we've opted to follow this
change. The migration path for this is as follows:

    - Update xorg-server, fontconfig.
    - Update fontconfig config files, /etc/fonts/fonts.conf in
      particular.
    - Upgrade all fonts packages, cave show fonts/*::/ should show all
      your installed fonts.
    - Update your /etc/X11/xorg.conf as follows: Every 'FontPath' line
      under the 'Files' section should be updated to point to a
      subdirectory of /usr/share/fonts/X11, rather than
      /usr/lib*/X11/fonts/.

Note that you will have to migrate your installation to use the new
location of fonts, even for older versions of X11.

