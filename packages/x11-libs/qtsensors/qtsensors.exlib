# Copyright 2017-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Qt Cross-platform application framework: QtSensors"
DESCRIPTION="
The Qt Sensors API provides access to sensor hardware via QML and C++
interfaces. The Qt Sensors API also provides a motion gesture
recognition API for devices."

LICENCES="FDL-1.3 GPL-2 GPL-3 LGPL-3"
MYOPTIONS="examples"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
"
# NOTE: Automagic dep on the unpackaged sensorfw:
# https://git.merproject.org/mer-core/sensorfw

qtsensors_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

