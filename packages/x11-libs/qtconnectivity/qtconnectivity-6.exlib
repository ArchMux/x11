# Copyright 2017-2018, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtBluetooth and QtNfc"
DESCRIPTION="
* The Bluetooth API provides connectivity between Bluetooth enabled devices.
Qt Bluetooth supports Bluetooth Low Energy development for client/central role
use cases.

* The NFC API provides connectivity between NFC enabled devices. NFC is an
extremely short-range (less than 20 centimeters) wireless technology and has a
maximum transfer rate of 424 kbit/s. NFC is ideal for transferring small
packets of data when two devices are touched together."

LICENCES+="
    LGPL-3
    GPL-2 [[ note = [ sdpcanner, src/tools/sdpscanner/qt_attribution.json ] ]]
"

MYOPTIONS="examples"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        net-wireless/bluez
        x11-libs/qtbase:${SLOT}[>=${PV}]
        examples? ( x11-libs/qtdeclarative:${SLOT}[>=${PV}] )
    suggestion:
        net/neard [[
            description = [ Runtime dep for QtN(ear)F(ield)C(ommunications) ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DFEATURE_bluez:BOOL=ON
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples QT_BUILD_EXAMPLES'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'examples Qt6Quick'
)

qtconnectivity-6_src_compile() {
    ninja_src_compile

    option doc && eninja docs
}

qtconnectivity-6_src_install() {
    ninja_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

