# Copyright 2015-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]
require python [ blacklist=2 multibuild=false ]
require flag-o-matic

export_exlib_phases src_configure src_prepare src_compile src_install

SUMMARY="Qt Cross-platform application framework: Integrate chromium into Qt"

LICENCES="
    FDL-1.3
    LGPL-3
"

MYOPTIONS="designer examples geolocation kerberos
    pipewire [[ description = [ Provides PipeWire support in WebRTC using GIO ] ]]
    pulseaudio
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.6]
        dev-lang/yasm
        dev-util/gperf
        dev-python/html5lib[python_abis:*(-)?]
        sys-devel/bison
        sys-devel/flex
        sys-devel/ninja[>=1.7.2]
        virtual/pkg-config
        x11-proto/xorgproto [[ note = glproto ]]
    build+run:
        app-arch/snappy
        app-spell/hunspell:=
        dev-lang/node[>=12.0]
        dev-libs/expat
        dev-libs/glib:2[>=2.32.0]
        dev-libs/icu:=[>=68]
        dev-libs/libevent:=
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        dev-libs/nspr[>=4.0]
        dev-libs/nss[>=3.26]
        dev-libs/re2:=
        media/ffmpeg[>=3.0]
        media-libs/fontconfig
        media-libs/freetype:2[>=2.4.2]
        media-libs/lcms2
        media-libs/libpng:=[>=1.6.0]
        media-libs/libvpx:=[>=1.10.0]
        media-libs/libwebp:=[>=0.4]
        media-libs/opus[>=1.3.1]
        sys-apps/dbus
        sys-apps/pciutils
        sys-libs/zlib
        sys-sound/alsa-lib[>=1.0.10]
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/harfbuzz[>=2.9.0]
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libxkbcommon[>=0.5.0]
        x11-libs/libxkbfile
        x11-libs/libXrandr
        x11-libs/libxshmfence
        x11-libs/libXScrnSaver
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/qtbase:${SLOT}[>=6.2][gui]
        x11-libs/qtdeclarative:${SLOT}[>=6.2]
        x11-libs/qtwebchannel:${SLOT}[>=6.2][qml]
        x11-libs/qtwebsockets:${SLOT}[>=6.2]
        designer? ( x11-libs/qttools:${SLOT}[>=6.2][designer] )
        geolocation? ( x11-libs/qtpositioning:${SLOT}[>=6.2] )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        pipewire? ( media/pipewire )
        providers:eudev? ( sys-apps/eudev )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:systemd? ( sys-apps/systemd )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.10] )
    run:
        examples? ( x11-libs/qtsvg:6 [[ note = [ for qtpdfquick example ] ]] )
"


qtwebengine-6_src_prepare() {
    cmake_src_prepare

    # TODO: Unfortunately the two sed invocations below aren't enough to
    # respect the selected python abi yet, but should be a step in the right
    # direction. Maybe providers.exlib could be another solution?
    edo sed -e "s/env python/env python$(python_get_abi)/" \
        -i src/3rdparty/chromium/third_party/catapult/tracing/bin/generate_about_tracing_contents

    edo sed -e "/script_executable =/s/python3/python$(python_get_abi)/" \
        -i src/3rdparty/chromium/.gn

    # cc, cxx, ar, ld, nm are passed via variables configured by cmake, but
    # CMAKE_READELF exists.
    edo sed -e "/nm = .*/a \ \ readelf = \"$(exhost --tool-prefix)readelf\"" \
        -i src/host/BUILD.toolchain.gn.in
}

qtwebengine-6_src_configure() {
    export NINJA_PATH=/usr/$(exhost --target)/bin/ninja
    export NINJAFLAGS="-v -j${EXJOBS:-1}"

    # Fails to build with
    # /usr/x86_64-pc-linux-gnu/bin/x86_64-pc-linux-gnu-ld: cannot find -lnss3
    # when linking libQt6WebEngineCore.
    append-ldflags "$($(exhost --tool-prefix)pkg-config --libs-only-L nss)"

    cmake_params+=(
        # Of course there's a NIH solution to see cmake's output during src_configure
        --log-level=STATUS
        -DPython3_EXECUTABLE:PATH=${PYTHON}
        -DFEATURE_qtpdf_build:BOOL=ON
        -DFEATURE_pdf_v8:BOOL=ON
        -DFEATURE_pdf_xfa:BOOL=ON
        -DFEATURE_pdf_xfa_bmp:BOOL=ON
        -DFEATURE_pdf_xfa_gif:BOOL=ON
        -DFEATURE_pdf_xfa_png:BOOL=ON
        -DFEATURE_pdf_xfa_tiff:BOOL=ON
        -DFEATURE_webengine_build_ninja:BOOL=OFF
        -DFEATURE_webengine_embedded_build:BOOL=ON
        -DFEATURE_webengine_extensions:BOOL=ON
        -DFEATURE_webengine_pepper_plugins:BOOL=ON
        -DFEATURE_webengine_printing_and_pdf:BOOL=ON
        -DFEATURE_webengine_proprietary_codecs:BOOL=ON
        -DFEATURE_webengine_spellchecker:BOOL=ON
        -DFEATURE_webengine_webchannel:BOOL=ON
        -DFEATURE_webengine_webrtc:BOOL=ON
        -DFEATURE_webengine_sanitizer:BOOL=OFF
        -DFEATURE_webengine_system_alsa:BOOL=ON
        -DFEATURE_webengine_system_ffmpeg:BOOL=ON
        -DFEATURE_webengine_system_freetype:BOOL=ON
        -DFEATURE_webengine_system_glib:BOOL=ON
        -DFEATURE_webengine_system_harfbuzz:BOOL=ON
        -DFEATURE_webengine_system_icu:BOOL=ON
        -DFEATURE_webengine_system_lcms2:BOOL=ON
        -DFEATURE_webengine_system_libevent:BOOL=ON
        -DFEATURE_webengine_system_libjpeg:BOOL=ON
        -DFEATURE_webengine_system_libpci:BOOL=ON
        -DFEATURE_webengine_system_libpng:BOOL=ON
        -DFEATURE_webengine_system_libvpx:BOOL=ON
        -DFEATURE_webengine_system_libwebp:BOOL=ON
        -DFEATURE_webengine_system_libxml:BOOL=ON
        -DFEATURE_webengine_system_opus:BOOL=ON
        -DFEATURE_webengine_system_re2:BOOL=ON
        # Only needed for tests, which we currently don't care about
        -DFEATURE_webengine_system_poppler:BOOL=OFF
        -DFEATURE_webengine_system_snappy:BOOL=ON
        $(cmake_disable_find designer Qt6Designer)
        $(cmake_option examples QT_BUILD_EXAMPLES)
        $(qt_cmake_feature geolocation webengine_geolocation)
        $(qt_cmake_feature kerberos webengine_kerberos)
        $(qt_cmake_feature pipewire webengine_webrtc_pipewire)
        $(qt_cmake_feature pulseaudio webengine_system_pulseaudio)
    )

    ecmake "${cmake_params[@]}"
}

qtwebengine-6_src_compile() {
    ninja_src_compile

    option doc && eninja docs
}

qtwebengine-6_src_install() {
    ninja_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs

    if option examples ; then
        edo rmdir "${IMAGE}"/usr/share/qt6/examples/pdf/{multipage,pdfviewer}
    fi
}

