# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg

SUMMARY="Xrestop uses the X-Resource extension to provide 'top' like statistics of each
connected X11 client's server side resource usage"
DESCRIPTION="
Xrestop uses the X-Resource extension to provide 'top' like statistics of each
connected X11 client's server side resource usage. It is intended as a
developer tool to aid more efficient server resource usage and debug server
side leakage.  It should work with any server supporting the X-Resource
extension, including the freedesktop.org server and XFree86 4.3+. 'xdpyinfo |
grep Resource' should tell you if your server supports this extension.
"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# NOTE: If we need to regenerate configure, this needs a build dep on
# x11-utils/util-macros.
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-libs/ncurses
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXres
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --hates=docdir --hates=datarootdir )

